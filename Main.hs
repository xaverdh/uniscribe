module Main where

import Text.Unicoder
import System.IO
import System.Environment (getArgs)
import System.Directory (getHomeDirectory)
import System.FilePath ((</>))

main :: IO ()
main = do
  args <- getArgs
  confPath <- case args of
    [] -> do
      home <- getHomeDirectory
      pure $ home </> ".uniscriberc"
    [conf] -> pure conf
    _ -> error "usage: uniscribe [config file (defaults to ~/.uniscriberc)]"
  mbConfig <- loadConfig confPath
  case mbConfig of
    Just config -> loop config
    Nothing -> putStrLn "error in config"
  where
    loop config = do
      s <- getLine
      putStrLn $ unicodizeStr config s
      loop config
